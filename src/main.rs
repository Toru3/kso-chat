use std::{
    io::{self, Read, Write},
    net, str, sync, thread,
};

fn main() {
    let tls = {
        let config = {
            let mut root_store = rustls::RootCertStore::empty();
            root_store.add_trust_anchors(webpki_roots::TLS_SERVER_ROOTS.iter().map(|ta| {
                rustls::OwnedTrustAnchor::from_subject_spki_name_constraints(
                    ta.subject,
                    ta.spki,
                    ta.name_constraints,
                )
            }));
            rustls::ClientConfig::builder()
                .with_safe_defaults()
                .with_root_certificates(root_store)
                .with_no_client_auth()
        };
        const SERVER_NAME: &str = "koukoku.shadan.open.ad.jp";
        let server_name = SERVER_NAME.try_into().unwrap();
        let conn = rustls::ClientConnection::new(sync::Arc::new(config), server_name).unwrap();
        let sock = net::TcpStream::connect((SERVER_NAME, 992)).unwrap();
        sync::Mutex::new(rustls::StreamOwned::new(conn, sock))
    };
    thread::scope(|s| {
        s.spawn(|| {
            let stdin = io::stdin();
            let mut buffer = String::new();
            while stdin.read_line(&mut buffer).is_ok() {
                let mut g = tls.lock().unwrap();
                writeln!(g, "{buffer}").unwrap();
                g.flush().unwrap();
            }
        });
        println!("接続しました");
        let mut buffer = vec![0; 4096].into_boxed_slice();
        while let Ok(n) = tls.lock().unwrap().read(&mut buffer) {
            if n == 0 {
                continue;
            }
            let s = str::from_utf8(&buffer[..n]).unwrap();
            if let (Some(p), Some(q)) = (s.find("\r\n\r\n>> "), s.find(" <<\r\n")) {
                println!("{}", &s[p + 4..q + 3]);
            }
            buffer[0] = 0;
        }
    });
}
